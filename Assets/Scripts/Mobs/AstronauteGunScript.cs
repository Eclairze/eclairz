using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronauteGunScript : Monster
{

    [SerializeField] GameObject body;
    [SerializeField] GameObject armCover;
    [SerializeField] GameObject gun;
    void Start()
    {
        this.pv = 5;
        this.range = 1;
        this.dmg = 1;
        this.speed = 5;
        this.position = gameObject.transform.position;
        this.targetPosition = target.transform.position;
    }

    internal override void Move()
    {
        if (state == State.CHASE)
        {
            targetPosition = new Vector2(target.transform.position.x, target.transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }
    }
    internal override void Animate()
    {
        
    }
    internal override void Attack()
    {
        state = State.ATTACK;
    }
    internal override void StopAttack()
    {
        if(targetOnTrigger == true)
        {
            Attack();
        }
        else
        {
            state = State.CHASE;
        }
    }
    internal override void Die()
    {
        
    }
}

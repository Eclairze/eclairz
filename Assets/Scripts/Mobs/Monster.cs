using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
    CHASE,ATTACK,STOP
}
public class Monster : MonoBehaviour
{
    internal int pv;
    internal float speed;
    internal int range;
    internal int dmg;
    internal Vector2 position;
    internal Vector2 targetPosition;
    internal bool targetOnTrigger = false;
    public GameObject target;
    public Rigidbody2D rb;
    internal State state;
    void Start()
    {
        state = State.CHASE;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(pv <= 0)
        {
            Die();
        }
        Animate();
        
    }
    protected virtual void FixedUpdate()
    {
        Move();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            targetOnTrigger = true;
            if ( state == State.CHASE)
            {
                Attack();
            }
            
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            targetOnTrigger = false;
        }
    }
    internal virtual void Move()
    {

    }
    internal virtual void Animate()
    {

    }
    internal virtual void Attack()
    {

    }
    internal virtual void StopAttack()
    {

    }
    internal virtual void Die()
    {

    }
   public Vector2 direction()
    {
        return targetPosition - rb.position.normalized;
    }
}

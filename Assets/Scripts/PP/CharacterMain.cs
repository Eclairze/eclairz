using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterMain : MonoBehaviour
{
    public float moveSpeed;
    public Camera mainCamera;

    public Rigidbody2D rb;
    public GameObject fire;

    public Animator anim;

    ObjectActivable currentObjectActivable ;

    public bool attack = false;
    public bool spellCast = false;
    private Vector2 moveDirection;
    private Vector2 lastMoveDirection;
    private Vector2 mousePosition;

    void Start()
    {
        currentObjectActivable = new ObjectAFireSpell();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        ProcessInputs();
        Animate();
    }

    protected virtual void FixedUpdate()
    {
        Move();
    }
    void ProcessInputs()
    {
        if (attack != true && spellCast != true)
        {
            float moveX = Input.GetAxisRaw("Horizontal");

            float moveY = Input.GetAxisRaw("Vertical");


            if ((moveX == 0 && moveY == 0) && moveDirection.x != 0 || moveDirection.y != 0)
            {
                lastMoveDirection = moveDirection;
            }

            moveDirection = new Vector2(moveX, moveY).normalized;

            if (Input.GetKeyDown("e"))
            {
                Attack();
            }
            else if (Input.GetKeyDown("a"))
            {
                SpellCast();
            }

        }
    }
    void Move()
    {
        if (attack != true && spellCast != true)
        {
            rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
    void Animate()
    {
        anim.SetFloat("DirectionX", moveDirection.x);
        anim.SetFloat("DirectionY", moveDirection.y);
        anim.SetFloat("MoveMagnitude", moveDirection.magnitude);
        anim.SetFloat("LastDirectionX", lastMoveDirection.x);
        anim.SetFloat("LastDirectionY", lastMoveDirection.y);
        anim.SetBool("Attack", attack);
        anim.SetBool("CastSpell", spellCast);
    }
    void Attack()
    {
        attack = true;
        lastMoveDirection = moveDirection;
    }
    void SpellCast()
    {
        spellCast = true;
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 aimDirection = mousePosition.normalized;
        lastMoveDirection = aimDirection;
    }
    void StopAttack()
    {
        attack = false;
    }
    void StopCast()
    {
        Instantiate(fire, mousePosition, Quaternion.identity);
        spellCast = false;
    }


}
